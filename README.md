# About

Project is a test task for mageticAI interview.

## Getting Started

* 1\. Create virtual environment
```
python -m venv env
```
* 2\. Activate virtual environment
```
env\Scripts\activate
```
* 3\. Install all dependecies
```
pip install -m requirements.txt
```
* 4\. Navigate html_parser directory
* 5\.
```
python run.py
```

### Todo list

* 1\. Add tests
* 2\. Game blocks, games should seperate classes
* 3\. Add simple flask for module 2
* 4\. Add documentation with Sphinx
* 5\. Create dockerfile for simple flask app