"""HTML parser module.

Used to parse data from https://play.google.com/store/apps/category/GAME

"""
# TODO (Ilia S):
# 1. Add tests
# 2. Game blocks, games should seperate classes
# 3. Add simple flask for module 2
# 4. Add documentation with Sphinx
# 5. Create dockerfile for simple flask app


import logging
import os
import abc
import requests
import attr

from bs4 import BeautifulSoup


URL = "https://play.google.com/store/apps/category/GAME"
CATEGORY_NAMES_CLASS = "sv0AUd"
CATEGORY_NAMES_TAG = "h2"
BLOCK_NAMES_CLASS = "Ktdaqe"
BLOCK_NAMES_TAG = "div"
GAME_NAME_TAG = "div"
GAME_NAME_CLASS = "WsMG1c nnK0zc"
OUTPUT_FORMAT = "APP/CATEGORY/GAMES/{}/{}"


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s,%(msecs)d %(levelname)s: %(message)s',
    datefmt='%H:%M:%S',
)


class SourceData(abc.ABC):
    """
    ABC class for data producers.
    """

    @abc.abstractmethod
    def get_data(self):
        """
        Used to get unprocessed data in string format.
        """
        return


@attr.s
class GetDataFromURLSource(SourceData):
    """
    Used to get text data from url.

    Attributes
    ----------
    url : str
        source of the url
    """
    url = attr.ib(repr=True, init=True)

    def get_data(self):
        return requests.get(self.url).text


SourceData.register(GetDataFromURLSource)


@attr.s
class BaseParsedObject:
    """
    Base bs4.Element
    """
    tag              = attr.ib(repr=True, init=True)
    block_name_class = attr.ib(repr=True, init=True)


@attr.s
class ParseDataFromHTMLText:
    """
    Used to parse data from text.

    Attributes
    ----------
    html_text : str
        unprocessed html source.
    """
    html_text     = attr.ib(repr=False, init=True)
    result        = attr.ib(repr=False, init=False)

    def __attrs_post_init__(self):
        """
        Post init objects.
        :return:
        """
        self.soup = BeautifulSoup(self.html_text, "lxml")

    @staticmethod
    def get_str_for_game(category_name, game_name):
        """
        Get str in OUTPUT_FORMAT.
        :param category_name:
        :param game_name:
        :return:
        """
        return OUTPUT_FORMAT.format(category_name, game_name)

    def run(self):
        """
        Main function,
        :return: Games and categories in OUTPUT_FORMAT format.
        """
        # getting game blocks by tag and class
        result = ""

        # TODO This all should be rewrite as
        # =============================
        # gamesBlock = BasicBlock(tag, class)
        # gameBlock = BasicBlock(tag, class)
        # categoryBlock = BasicBlock(tag, class)
        # category_name = gameBlock.find(categoryBlock)
        # games = gameBlock.find(gameBlock)
        # print(games)

        game_blocks = self.soup.find_all(BLOCK_NAMES_TAG, {'class': BLOCK_NAMES_CLASS})
        # iterate over first 7 blocks
        for game_block in game_blocks[:7]:
            # get category name
            category_name = game_block.find_all(CATEGORY_NAMES_TAG, {'class': CATEGORY_NAMES_CLASS})
            # get games
            games = game_block.find_all(GAME_NAME_TAG, {'class': GAME_NAME_CLASS})
            # get temp results with generator
            temp_result = os.linesep.join((
                                              self.get_str_for_game(category_name[0].text, game.text) for game in games
            ))
            # add result to result strings
            result = os.linesep.join([temp_result, result])

        # Return str in OUTPUT_FORMAT.
        return result


if __name__ == "__main__":
    # Create data reader/producer
    dataProducer = GetDataFromURLSource(URL)
    # Get HTML data from producer
    html = dataProducer.get_data()
    # Create HTML paarser
    dataParser = ParseDataFromHTMLText(html)
    # Run parser and print result
    logging.info("Result: {}".format(dataParser.run()))
